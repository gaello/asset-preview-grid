﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom inspector drawer for AssetPreviewGrid script.
/// Adds button to the Inspector and menu item.
/// </summary>
[CustomEditor(typeof(AssetPreviewGrid))]
public class AssetPreviewGridEditor : Editor
{
    /// <summary>
    /// Attaches the component to the selected game object.
    /// </summary>
    [MenuItem("Custom Tools/Add Asset Preview Grid")]
    public static void AttachComponent()
    {
        // Check if only one game object is selected.
        if (Selection.gameObjects.Length != 1)
        {
            Debug.Log("Select one game object to attach AssetPreviewGrid script!");
            return;
        }

        // Get selected object.
        var selected = Selection.gameObjects[0];

        // Try to get AssetPreviewGrid script.
        AssetPreviewGrid placer = selected.GetComponent<AssetPreviewGrid>();

        // Check if selected object has this script and add if needed.
        if (!placer)
        {
            placer = selected.AddComponent<AssetPreviewGrid>();
        }
        else
        {
            Debug.LogWarning("Selected game object already have AssetPreviewGrid script attached!");
        }

        // Rearange elements.
        placer.RearrangeElements();
    }

    /// <summary>
    /// Unity method which draws Inspector.
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        // Add space between parameters and the button.
        EditorGUILayout.Space();

        // Button for rearranging elements.
        if (GUILayout.Button("Rearrange elements"))
        {
            var placer = target as AssetPreviewGrid;
            placer.RearrangeElements();
        }
    }
}
